<?php

namespace App\Controller;

use App\Repository\EntryRepository;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Routing\Annotation\Route;

class ForwardController extends AbstractController
{
  #[Route('/n/{id}', name: 'forward_number')]
  public function forward_number(int $id, EntryRepository $er): Response
  {
    $entry = $er->findOneById($id);
    if($entry != null) {
      $url = $entry->getUrl();
      return new RedirectResponse($url);
    }
    return new RedirectResponse('/');
  }

  #[Route('/k/{keyword}', name: 'forward_keyword')]
  public function forward_keyword(string $keyword, EntryRepository $er): Response
  {
    $entry = $er->findOneByKeyword($keyword);
    if($entry != null) {
      $url = $entry->getUrl();
      return new RedirectResponse($url);
    }
    return new RedirectResponse('/');
  }

  #[Route('/{keyword}', name: 'forward')]
  public function fw(string $keyword, EntryRepository $er): Response
  {
    if(ctype_digit($keyword)) {
      return $this->forward_number((int) $keyword, $er);
    }
    return $this->forward_keyword($keyword, $er);
  }
}
