<?php

namespace App\Controller;

use App\Entity\Entry;
use App\Repository\EntryRepository;

use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class MainController extends AbstractController
{
    #[Route('/', name: 'main')]
    public function index(): Response
    {
        return $this->render('main/index.html.twig');
    }

    #[Route('/api/insert', name: 'insert')]
    public function insert(Request $request, EntryRepository $er, ManagerRegistry $doctrine): Response
    {
        $data = $request->getContent();
        $data = json_decode($data);
        $link = '';

        $blocked_urls = array(
          'pxlme',
          'goo.by',
          'pixelfly.me',
          'stonikon.com',
          'yugt'
        );

        foreach($blocked_urls as $blocked) {
          if( strpos($data->url, $blocked) !== false ) {
            return $this->json([
              'status' => 'error',
              'message' => 'This URL is not allowed',
              'link' => $data->url
            ]);
          }
        }

        $em = $doctrine->getManager();

        if($data->mode == 'number') {
          $entry = new Entry();
          $entry->setUrl($data->url);
          $dt = new \DateTime;
          $timestamp = $dt->format('u');
          $entry->setKeyword($timestamp);
          $em->persist($entry);
          $em->flush();
          $entry = $er->findOneByKeyword($timestamp);
          $id = $entry->getId();
          $link = 'https://s.aschoch.ch/' . $id;
        } else {
          $existing = $er->findByKeyword($data->keyword);
          if($existing != null) {
            return $this->json([
              'status' => 'error',
              'message' => 'the keyword "' . $data->keyword . '" already exists. Please choose another.',
            ]);
          }
          $entry = new Entry();
          $entry->setUrl($data->url);
          $entry->setKeyword($data->keyword);
          $em->persist($entry);
          $em->flush();
          $link = 'https://s.aschoch.ch/' . $data->keyword;
        }

        return $this->json([
          'status' => 'OK',
          'message' => 'Your shortened URL:',
          'link' => $link
        ]);
    }
}
