import React from 'react';
import * as ReactDOM from 'react-dom/client';
import axios from 'axios';

import {
	Alert,
	Box,
	Button,
	Container,
	Grid,
	Dialog,
	DialogActions,
	DialogContent,
	DialogContentText,
	DialogTitle,
	Snackbar,
	TextField,
	ToggleButton,
	ToggleButtonGroup,
	Tooltip,
	Typography,
} from '@mui/material';

import {ThemeProvider} from '@mui/material/styles';
import { styled } from '@mui/material/styles';
import theme from './theme';

import ContentCopyIcon from '@mui/icons-material/ContentCopy';
import InfoIcon from '@mui/icons-material/Info';

class Main extends React.Component {
  constructor(props) {
    super(props);

		this.state = {
			url: '',
			mode: 'number',
			keyword: '',
			snackbar: false,
			errorMessage: '',
			dialog: false,
			link: '',
			copied: false,
		}

		this.handleSubmit = this.handleSubmit.bind(this);
		this.snackbar = this.snackbar.bind(this);
	}

  async handleSubmit() {
    if(this.state.url != '') {
			if(this.state.mode == 'keyword' && this.state.keyword == '') {
				this.snackbar('you have to enter a keyword');
				return;
			}
			axios.put('/api/insert', this.state).then((response) => {
				if(response.data.status != 'OK') {
					this.snackbar(response.data.message);
				} else {
					this.setState({
						dialog: true,
						link: response.data.link,
					});
				}
			});
    } else {
			this.snackbar('you have to enter a URL');
			return;
    }
	}

	snackbar(message) {
		this.setState({
			snackbar: true,
			errorMessage: message,
		});
	}

	render() {
		return (
      <ThemeProvider theme={theme}>
				<Container>
					<Typography variant="h3" style={{ marginTop: '50px', marginBottom: '20px', color: theme.palette.primary.dark }}>Shorten your URL</Typography>
					<Grid container spacing={2}>
						<Grid item xs={12}>
							<TextField label="https://..." variant="outlined" value={this.state.url} onChange={(e) => {this.setState({url: e.target.value})}} fullWidth/>
						</Grid>
						<Grid item xs={12}>
							<ToggleButtonGroup
								color="primary"
								value={this.state.mode}
								exclusive
								onChange={(e) => {this.setState({mode: e.target.value})}}
								fullWidth
							>
									<ToggleButton value="number">
										Use A Generated Number
									</ToggleButton>
									<ToggleButton value="keyword">
										Use A Keyword
									</ToggleButton>
							</ToggleButtonGroup>
						</Grid>
						{this.state.mode == 'keyword' && 
						<Grid item xs={12}>
							<TextField label="keyword" variant="outlined" value={this.state.keyword} onChange={(e) => {this.setState({keyword: e.target.value})}} fullWidth/>
						</Grid>}
						<Grid item xs={12}>
							<Button 
								variant="contained"
								onClick={this.handleSubmit}
								style={{ backgroundColor: theme.palette.primary.dark }}
							>
								Get your short Link!
							</Button>
						</Grid>
					</Grid>
					<Snackbar open={this.state.snackbar} autoHideDuration={6000} onClose={() => {this.setState({snackbar: false});}}>
						<Alert severity="error" sx={{ width: '100%' }}>
							{this.state.errorMessage}
						</Alert>
					</Snackbar>
					<Snackbar open={this.state.copied} autoHideDuration={6000} onClose={() => {this.setState({copied: false});}}>
						<Alert severity="success" sx={{ width: '100%' }}>
							Copied!
						</Alert>
					</Snackbar>
					<Dialog
						open={this.state.dialog}
						onClose={() => {this.state.dialog && this.setState({dialog: false})}}
						aria-labelledby="alert-dialog-title"
						aria-describedby="alert-dialog-description"
					>
						<DialogTitle variant="h5">Your shortened Link</DialogTitle>
						<DialogContent>
							<DialogContentText variant="h6">
								{this.state.link}
								<Button
									onClick={() => {navigator.clipboard.writeText(this.state.link) && this.setState({copied: true})}}
									style={{ marginLeft: '2px' }}
								>
									<ContentCopyIcon/>
								</Button>
							</DialogContentText>
							<DialogContentText variant="h6">
								<span style={{ userSelect: 'none', color: 'white' }}>https://</span>{this.state.link.replace("https://", "")}
								<Button
									onClick={() => {navigator.clipboard.writeText(this.state.link.replace("https://", "")) && this.setState({copied: true})}}
									style={{ marginLeft: '2px' }}
								>
									<ContentCopyIcon/>
								</Button>
							</DialogContentText>
						</DialogContent>
						<DialogActions>
							<Button
								onClick={() => {this.setState({dialog: false})}}
							>
								Close
							</Button>
						</DialogActions>
					</Dialog>
				</Container>
			</ThemeProvider>
		);
	}
}

const container = document.getElementById('main');
const root = ReactDOM.createRoot(container);
root.render(<Main/>);
