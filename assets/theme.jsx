import {createTheme, ThemeProvider} from '@mui/material/styles';

const fonts = createTheme({
  typography: {
    fontFamily: [
      'cmu',
    ].join(','),
  },
});

const theme = createTheme({
  typography: {
    fontFamily: [
      'cmu',
    ].join(','),
  },
  palette: {
    type: 'light',
    primary: {
      main: '#D81C23',
      light: '#4caf50',
      contrastText: '#ffffff',
      dark: '#810409',
    },
    secondary: {
      main: '#b71c1c',
      light: '#f7c870',
      dark: '#560c0c',
    },
  },
});

export default theme;
