# URL shortener

A Symfony/React application to shorten your links

## Public Instance

You can try the application on https://shorten.aschoch.ch. 

## CLI

run `./shorten-cli -n URL` or `./shorten-cli -k KEYWORD URL` from your command line. 

`shorten-cli` depends on the `curl` and `jq` packages.
